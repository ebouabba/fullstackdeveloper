# FullStackDeveloper

#   Cheatsheet: HTML5 Overview

This tag denotes a comment in HTML, which is not displayed by a browser but can be useful to hide and document code.	

<!-- This is a comment -->

All HTML documents must start with this declaration. It tells the browser what document type to expect. Note that this element has no ending tag.	


<!DOCTYPE html>
<html>
    <head>
      <!-- Metadata Here -->
    </head>
    <body>
        <!-- Document Body Here -->
    </body>
</html>


This tag, called an “anchor tag” creates hyperlinks using the href attribute. In place of path enter the URL or path name to the page you want to link to.	


<a href="https://www.ibm.com">IBM</a>

Contains the contents of the HTML document. It should contain all other tags besides the <head> element to display the body of the document.	

<!DOCTYPE html>
<html>
  <head>
    <!-- Metadata Here -->
  </head>
  <body>
      <!-- Document Body Here -->
  </body>
</html>

Often used to separate sections in the body of a document in order to style that content with CSS.	


<div>
This element has no particular semantic meaning but is often used in conjunction with CSS for styling purposes.
</div>


Adds a level 1 heading to the HTML document.	


<h1>Thomas J. Watson</h1>

Contains metadata and should be placed after the <html> tag and before the <body> tag.	


<!DOCTYPE html>
<html>
    <head>
        <!-- Metadata Here -->
    </head>
    <body>
        <!-- Document Body Here -->
    </body>
</html>


The root element of an HTML document. All other tags in the document should be contained in this tag.	


<!DOCTYPE html>
<html>
  <head>
     <!-- Metadata Here -->
  </head>
  <body>
      <!-- Document Body Here -->
  </body>
</html>


This tag is used to place an img. In place of path insert a URL or a relative file path to the image location. Other optional attributes include width and height of the image in pixels.	


<img src=“https://upload.wikimedia.org/wikipedia/commons/7/7e/Thomas_J_Watson_Sr.jpg” width=“300” height=“300”/>


Element that creates bulleted line items in an ordered or unordered list.  Should be used in conjunction with the <ul> or <ol> tags.	


<ul>
    <l1>Bullet point 1<l1>
    <l1>Bullet point 2<l1>
</ul>


Used to link an external document, such as a CSS file, to an HTML document.	


<head>
    <link rel=“stylesheet” href=“styles.css”>
</head>


Used to provide metadata about the HTML document.	


<head>
    <meta name=“author” content=“Christopher Moore”>
</head>

Element that creates an ordered list using numbers. Should be used in conjunction with the <li> tag.	


<ol>
    <li>Numbered bullet point 1<li>
    <li>Numbered bullet point 2<li>
</ol>


This tag is used to identify a paragraph. It places a line break after the text it is enclosed in.	


<p>This is a paragraph of text. It can be as short or as long as needed.</p>


Used to embed JavaScript in an HTML document.	


<script>
    alert(“Hello World”);
</script>


This tag is used to denote a table. Should be used with <tr> (defines a table row) and <td> (defines a table cell within a row) tags. The <th> tag can also be used to define the table header row.	


<table>
  <tr>
    <th>Header cell 1</th>
    <th>Header cell 2</th>
  </tr>
  <tr>
    <td>First row first cell</td>
    <td>First row second cell</td>
  </tr>
  <tr>
    <td>Second row first cell</td>
    <td>Second row second cell</td>
  </tr>
</table>

</td>


Denotes a cell within a row, within a table.	


<td>Cell Content</td>


Denotes the header cells within a row within a table.	


<table>
  <tr>
    <th>Header cell 1</th>
    <th>Header cell 2</th>
  </tr>
  <tr>
    <td>First row first cell</td>
    <td>First row second cell</td>
  </tr>
  <tr>
    <td>Second row first cell</td>
    <td>Second row second cell</td>
  </tr>
</table>


Defines the title of the HTML document displayed in the browser’s title bar and tabs. It is required in all HTML documents. It should be contained in the <head> tag.	


<!DOCTYPE html>
<html>
    <head>
        <title>Document Title</title>
    </head>
    <body>
        <!-- Document Body Here -->
    </body>
</html>


Denotes a row within a table.	


<table>
  <tr>
    <th>Header cell 1</th>
    <th>Header cell 2</th>
  </tr>
  <tr>
    <td>First row first cell</td>
    <td>First row second cell</td>
  </tr>
  <tr>
    <td>Second row first cell</td>
    <td>Second row second cell</td>
  </tr>
</table>


Element that creates an unordered list using bullets. Should be used in conjunction with the
tag.


<ul>
  <li>Bullet point 1<li>
  <li>Bullet point 2<li>
</ul>




The Web Server
We talked about how the front-end consists of the information sent to a client so that a user can see and interact with a website, but where does the information come from? The answer is a web server.

The word “server” can mean a lot of things in computing, but we’re going to focus on web servers specifically. A web server is a process running on a computer that listens for incoming requests for information over the internet and sends back responses. Each time a user navigates to a website on their browser, the browser makes a request to the web server of that website. Every website has at least one web server. A large company like Facebook has thousands of powerful computers running web servers in facilities located all around the world which are listening for requests, but we could also run a simple web server from our own computer!

The specific format of a request (and the resulting response) is called the protocol. You might be familiar with the protocol used to access websites: HTTP. When a visitor navigates to a website on their browser, similarly to how one places an order for takeout, they make an HTTP request for the resources that make up that site.

For the simplest websites, a client makes a single request. The web server receives that request and sends the client a response containing everything needed to view the website. This is called a static website. This doesn’t mean the website is not interactive. As with the individual static assets, a website is static because once those files are received, they don’t change or move. A static website might be a good choice for a simple personal website with a short bio and family photos. A user navigating Twitter, however, wants access to new content as it’s created, which a static website couldn’t provide.

A static website is like ordering takeout, but modern web applications are like dining in person at a sit-down restaurant. A restaurant patron might order drinks, different courses, make substitutions, or ask questions of the waiter. To accomplish this level of complexity, an equally complex back-end is required.







Learn
WHAT IS THE BACK-END?
Review
In order to deliver the front-end of a website or web application to a user, a lot needs to happen behind the scenes on the back-end! Understanding what makes up the back-end can be overwhelming because the back-end has a lot of different parts, and different websites or web applications can have dramatically different back-ends. We covered a lot in this lesson, so let’s review what we learned:

The front-end of a website or application consists of the HTML, CSS, JavaScript, and static assets sent to a client, like a web browser.
A web server is a process running on a computer somewhere that listens for incoming requests for information over the internet and sends back responses.
Storing, accessing, and manipulating data is a large part of a web application’s back-end
Data is stored in databases which can be relational databases or NoSQL databases.
The server-side of a web application, sometimes called the application server, handles important tasks such as authorization and authentication.
The back-end of web application often has a web API which is a way of interacting with an application’s data through HTTP requests and responses.
Together the technologies used to build the front-end and back-end of a web application are known as the stack, and many different languages and frameworks can be used to build a robust back-end.
Now that you have a sense for server-side web development and what the back-end is, you’re ready to dive in and learn about the different parts in more depth!






